#include <iostream>
#include <chrono>
#include <iomanip>
#include <boost/any.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/signals2.hpp>

int main() {
        std::string name = "GitLab";
        std::string emoji = "🦊";

        auto gl_init = std::chrono::system_clock::from_time_t(1318109798);
        auto gl_target = std::chrono::system_clock::from_time_t(1633728998);

        // std::chrono::years requires C++20
        auto gl_party_duration = std::chrono::duration_cast<std::chrono::years>(gl_target - gl_init).count();

        auto gl_party_time = std::chrono::system_clock::to_time_t(gl_target);

        std::cout << "Get the party started for " << gl_party_duration << " years "
                << boost::format("%1% %2%") % name % emoji << " on "
                << std::put_time(std::localtime(&gl_party_time), "%c %Z") << std::endl;

        return 0;
}
